class Article < ActiveRecord::Base
  def mark(user_id, read = -1, archive = -1)
    userarticle = UserArticle.find(:first, 
                      :conditions => ['"user" = ? AND article = ?', user_id, id])
    if not userarticle
      userarticle = UserArticle.new
    end
    User.count_by_sql(["DELETE FROM user_articles WHERE \"user\" = ? AND \"article\" = ?", user_id, id])
    if archive != -1
      userarticle.archive = archive
    end
    if read != -1
      userarticle.read = read
    end
    if not (userarticle.read or userarticle.archive) then return end
    User.count_by_sql(['INSERT INTO user_articles ("article", "user", "archive", "read")
       VALUES(?, ?, ?, ?)', id, user_id, userarticle.archive, userarticle.read])
  end
end
