class User < ActiveRecord::Base
  @_feeds = nil
  @_articles = nil
  @_unread = nil
  @_unread_count = nil
  def self.authenticate(name, password)
    find(:first, :conditions => [ "username = ? AND password = ?", name, password ])
  end
  def feeds(feed_id = -1)
    if not @_feeds
      @_feeds = Feed.find(:all, 
                          :joins => "INNER JOIN user_feeds ON feeds.id = user_feeds.feed",
                          :conditions =>  ["user_feeds.user = ?", id] )
      @_feeds.sort! do |a, b| a.id <=> b.id end
    end
    if feed_id == -1
      return @_feeds
    else
      for feed in @_feeds
        if feed.id == feed_id.to_i
          return feed
        end
      end
    end
    return nil
  end
  def articles(feed_id = -1)
    if not @_articles and feed_id == -1
      @_articles = Article.find(:all, 
                                :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                                :order => "date DESC",
                                :joins => "RIGHT JOIN (SELECT articles.* FROM articles 
                                            INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                                            WHERE user_feeds.user = #{id}) as articles 
                                ON articles.id = user_articles.article")
      return @_articles
    elsif feed_id == -1
      return @_articles
    else
      return Article.find(:all, 
                        :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                        :order => "date DESC",
                        :joins => "RIGHT JOIN (SELECT * FROM articles 
                          INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                          WHERE user_feeds.user = #{id}) as articles 
                        ON articles.id = user_articles.article",
                        :conditions => ["articles.feed_id = ?", feed_id])
    end
  end
  def article(article_id)
    return Article.find(:first, 
                        :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                        :joins => "RIGHT JOIN (SELECT articles.* FROM articles 
                          INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                          WHERE user_feeds.user = #{id}) as articles 
                        ON articles.id = user_articles.article",
                        :conditions => ["articles.id = ?", article_id])

  end
  def unread(feed_id = -1)
    if not @_unread
      @_unread = Article.find(:all, 
                              :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                              :order => "date DESC",
                              :joins => "RIGHT JOIN (SELECT articles.* FROM articles 
                                INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                                WHERE user_feeds.user = #{id}) as articles 
                              ON articles.id = user_articles.article",
                              :conditions => ["user_articles.read IS NOT TRUE", id])
    end
    if feed_id == -1
      return @_unread
    else
      return @_unread.select {|article| article.feed_id == feed_id}
    end
  end
  def unread_count(feed_id = -1)
    sum = 0
    if not @_unread
      if not @_unread_count
        @_unread_count = Article.find(:all, 
                                :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                                :select => 'feed_id',
                                :joins => "RIGHT JOIN (SELECT articles.* FROM articles 
                                  INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                                  WHERE user_feeds.user = #{id}) as articles 
                                ON articles.id = user_articles.article",
                                :conditions => ["user_articles.read IS NOT TRUE", id])
      end
      @_unread_count.each {|article| if article.feed_id == feed_id or feed_id == -1 then sum += 1 end}
    else
      @_unread.each {|article| if article.feed_id == feed_id or feed_id == -1 then sum += 1 end}
    end
    return sum
  end
  def archive(feed_id = -1)
    if not @_articles and feed_id == -1
      @_articles = Article.find(:all, 
                                :order => "date DESC",
                                :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                                :joins => ["RIGHT JOIN (SELECT articles.* FROM articles 
                                            INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                                            WHERE user_feeds.user = #{id}) as articles 
                                ON articles.id = user_articles.article"],
                                :conditions => ["user_articles.archive = 't'", id])
      return @_articles
    else
    return Article.find(:all, 
                        :from => "(SELECT * FROM user_articles WHERE user_articles.user = #{id}) as user_articles",
                        :order => "date DESC",
                        :joins => "RIGHT JOIN (SELECT articles.* FROM articles 
                          INNER JOIN user_feeds ON user_feeds.feed = articles.feed_id 
                          WHERE user_feeds.user = #{id}) as articles 
                        ON articles.id = user_articles.article",
                        :conditions => ["user_articles.archive = 't' AND 
                                        articles.feed_id = ?", feed_id])
    end
  end
end

