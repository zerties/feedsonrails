class Feed < ActiveRecord::Base
  def has_tag?(tag)
    if attributes.keys.include? "tags"
      tager = tags.split(";").map { |d| d.strip }
      return tager.include?(tag)
    end
  end
  def get_tags
    if attributes.keys.include? "tags"
      return tags.split(";").map { |d| d.strip }
    end
  end
  def real_title
    if attributes.keys.include? "force_title" and force_title != "" and force_title
      return force_title
    else
      return title
    end
  end
end

class MetaFeed < Feed
  attr_accessor :shown
  attr_accessor :tags
  attr_accessor :force_title
  attr_accessor :url
  attr_accessor :id
  attr_accessor :link
  attr_accessor :title

  def initialize(user_id)
    @shown = true
    @title = ""
    @force_title = ""
    @tags = ""
    @url = ""
    @user = user_id
  end
  def populate(feed)
    if feed.attributes.keys.include? "shown"
      if feed.shown == "t"
        @shown = true
      else
        @shown = false
      end
      @tags = feed.tags.to_s
      @force_title = feed.force_title.to_s
    end

    @id = feed.id
    @title = feed.title
    @url = feed.url
    @link = feed.link
    self
  end
  def save
    # save Feed if there isn't already one
    feed = Feed.find(:first, :conditions => ["url = ?", @url])
    if not feed
      feed = Feed.new(:title => @title, :url => @url, :link => @link)
      feed.save
    end
    # Update user_feed or create it
    if @id.to_i != -1
      User.count_by_sql(["DELETE FROM user_feeds WHERE \"user\" = ? and \"feed\" = ?",
                        @user, @id])  
    end
    User.count_by_sql(['INSERT INTO 
                      user_feeds ("user", "feed", "tags", "shown", "force_title")
                      VALUES(?, ?, ?, ?, ?)', @user, feed.id, @tags, @shown, @force_title])
    @id = feed.id
  end
  def delete
    User.count_by_sql(["DELETE FROM user_feeds WHERE \"user\" = ? and \"feed\" = ?",
                      @user, @id])
  end
end

