class FiltersController < ApplicationController
  before_filter :authenticate
  def initialize 
    @links = []
  end
  def index
    @filters = Filter::of_user(@user)
  end
  def new
    @action = "Create"
    @filter = Filter.new
    render :action => "edit"
  end
  def edit  
    @action = "Edit"
    @filter = Filter.find(params[:id])
    if @filter.owner_id != @user.id
      @filer = nil
    end
  end
  def update
    if params[:id] and params[:id] != ""
      filter = Filter.find(params[:id].to_i)
    end
    if not filter
      filter = Filter.new
      filter.owner_id = @user.id
    end
    filter.feed_id = params[:feed_id].to_i
    filter.title = params[:title]
    filter.content = params[:content]
    filter.link = params[:link]
    filter.closures = params[:closures]
    filter.target = params[:target]
    if not filter.owner_id == @user.id 
      flash[:error] = "You can't edit the filter of another user"
      render :controller => "feeds", :action => "list"
      return
    end
    if not @user.feeds(filter.feed_id) and filter.feed_id != 0
      flash[:error] = "No such feed"
      @filter = filter
      @action = "Edit"
      render :action => "edit"
      return
    end
    if not filter.target or filter.target == ""
      flash[:error] = "Please specify an target"
      @filter = filter
      @action = "Edit"
      render :action => "edit"
      return
    end
    filter.save  
    flash[:notice] = "Update was successful"
    index
    render :action=>"index"
  end
  def destroy 
    if params[:id] and params[:id] != ""
      filter = Filter.find(params[:id].to_i)
    end
    if not filter.owner_id == @user.id 
      flash[:error] = "You can't edit the filter of another user"
      render :controller => "feeds", :action => "list"
      return
    end
    filter.destroy
    flash[:notice] = "Filter was destroyed"
    index
    render :action => "index"

  end
end
