class FeedsController < ApplicationController
  layout 'feeds', :except => :export
  before_filter :authenticate

  def initialize
    @links = []
  end
  def index
    list
    render :action => "list"
  end
  def list
    if params[:tag]
      @feeds = @user.feeds.select { |feed|
        feed.has_tag?(params[:tag]) 
      } 
    else
      @feeds = @user.feeds.reject { |feed|
        not (feed.shown == "t" or 0 != @user.unread_count(feed.id))
      }
    end
    @feeds.sort! { |a,b| a.id <=> b.id }
  end
  def read
    if params[:id] 
      @feed = Feed.find(params[:id])
      if params[:all]
        @articles = @user.articles(params[:id])
      else
        @articles = @user.unread(@feed.id)
      end
      if @articles.size == 0
        redirect_to :action => "list"
      end
    elsif params[:tag]
      @articles = []
      @user.feeds.each do |feed|
        if feed.has_tag?(params[:tag])
          @articles += @user.unread(feed.id)
        end
      end
    else 
      @articles = @user.unread(-1)
    end
  end
  def new
    @feed = Feed.new
    @feed = -1
    @feed = MetaFeed.new(@user.id)
    render :action => "edit" and return
  end
  def preview_feed
    articles = Article.find(:all, :limit => 3, :order => "date DESC", :conditions => ["feed_id = ?", params[:id]])
    text = ''
    if articles[0]
      text += "<h3>" + articles[0].title + "</h3>"
      if articles[0].text
        text += articles[0].text.split(" ")[0..30].join(" ") + "..."
      end
    end
    if articles[1]
      text += "<h4>" + articles[1].title + "</h4>"
    end
    if articles[2]
      text += "<h5>" + articles[2].title + "</h5>"
    end
    render :text => text
  end
  def destroy
    if params[:id]
      @feed = Feed.new
      @feed = MetaFeed.new(@user.id)
      @feed.id = params[:id]
      @feed.delete
    end
    redirect_to :back
  end
  def change_password
    if not (params[:old_password] and params[:new_password] and params[:new_password2])
      flash[:error] = "Please fill all fields"
      extended
      render :action => "extended"
      return
    end
    if not (params[:new_password] == params[:new_password2])
      flash[:error] = "Type the new password two times correct"
      extended
      render :action => "extended"
      return
    end
    if not @user.password == params[:old_password]
      flash[:error] = "The old password isn't correct"
      extended
      render :action => "extended"
      return
    end
    @user.password = params[:new_password]
    @user.save
    flash[:notice] = "Password successfully changed"
    extended
    render :action => "extended"
  end
  def edit
    if not (feed = @user.feeds(params[:id]))
      feed = Feed.find(params[:id])
    end
    @feed = MetaFeed.new(@user.id)
    @feed.populate(feed)
  end
  def update
    Feed.find(:first)
    feed = MetaFeed.new(@user.id)
    feed.id = params[:id].to_i
    feed.url = params[:url].strip
    feed.tags = params[:tags]
    feed.force_title = params[:force_title].strip
    if params[:shown] == 'true'
      feed.shown = true
    else
      feed.shown = false
    end
    if not feed.url or feed.url == ""
      flash[:error] = "You have to set an url"
      @feed = feed
      render :action => "edit" and return
    end
    feed.save
    redirect_to :action => "list"
  end
  # Mark article/tag/evrything as read
  def markasread
    value = true
    if params[:value] and params[:value] == 'unread' then value = false end
    if params[:article_id]
      article = Article.find(params[:article_id])
      article.mark(@user.id, value)
      if not params[:ajax] 
        redirect_to :back
      else
        render :text=>'marked'+value.to_s
      end
      return
    elsif params[:feed_id]
      if params[:feed_id] == "all"
        feeds = @user.feeds(-1)      
      else
        feeds = [@user.feeds(params[:feed_id])]
      end
      feeds.each { |feed|
        @user.articles(feed.id).each { |article|
          article.mark(@user.id, value)
        }
      }
      redirect_to :action => :list and return
    elsif params[:articles]
      params[:articles].split(";").each { |article|
        article = Article.find(article)
        article.mark(@user.id, value)
      }
      redirect_to :action => "list" and return
    end  
  end
  def archiv
    if params[:id]
      article = @user.article(params[:id])
      if not (article.archive == "t")
        article.mark(@user.id, -1, true)
      else 
        article.mark(@user.id, -1, false)
      end
      redirect_to :back
    else
      # Show the archiv
      @articles = @user.archive(-1)
      render :action => "read"
    end
  end
  def goto
    @article = Article.find(params[:id])
    @article.mark(@user.id, true)
    if @article.url
      redirect_to @article.url
    else
      render :text => "There is no link, probably this is a bug"
    end
  end
  def extended
    @feeds = @user.feeds
    @feeds.sort! { |a,b| a.id <=> b.id }
  end
  # Searching for a Regex
  def search
    if params[:search]
      search = params[:search]
      regex = Regexp.new(search['keyword'], Regexp::IGNORECASE)
      if search['fulltext'] != 'yes'
        @articles = @user.articles.select { |a| a.title =~ regex }
      else
        @articles = @user.articles.select { |a| 
          (a.title.to_s + a.text.to_s) =~ regex }
      end
      @articles.map! { |a|
        a.text.gsub!(regex) { |t| "<span style='color: #0f0;'>#{t}</span>"  }
        a.title.gsub!(regex) { |t| "<span style='color: #0f0;'>#{t}</span>" }
        a
      }
      flash[:notice] = "#{@articles.size} Articles found"
      render :action => "read"
    end
  end
  def tags
    @tags = {}
    @user.feeds.each do |feed|
      feed.get_tags.each do |tag|
        if not @tags[tag] 
          @tags[tag] = @user.unread_count(feed.id)
        else
          @tags[tag] += @user.unread_count(feed.id)
        end
      end
    end
  end
  def export
    @feeds = @user.feeds 
  end
  def import
    Feed.new
    if params[:opmlfile]
      document = REXML::Document.new(params[:opmlfile].read)
      body = document.root.elements['body']
      @imported = []
      @unimported = []
      body.each_element_with_attribute('xmlUrl') do |outline|
        feed = MetaFeed.new(@user.id)
        yesno = proc { |bool| not ["false", "f"].include? bool }
            
        feed.force_title = outline.attributes['force_title']

        if outline.attributes['xmlUrl']
          feed.url = outline.attributes['xmlUrl']
          if @user.feeds.select {|f| f.url == feed.url } != []
            next
          end
        else
          next
        end
        
        feed.tags = outline.attributes['tags']
        feed.shown = yesno.call(outline.attributes['shown'])

        feed.save
      end
    else
      flash[:error] = "You haven't uploaded an opml file"
      extended
      render :action => "extended"
    end
  end
end
