# Filters added to this controller will be run for all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
class ApplicationController < ActionController::Base
  protected
  def authenticate
    unless session["id"]
      redirect_to :controller => "login" 
      return false
    end
    @user = User.find(session['id'])
  end
end
