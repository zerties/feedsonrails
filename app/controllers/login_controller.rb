class LoginController < ApplicationController
  def index
    # show login screen
  end
  def login
    if person = User.authenticate(params["name"], params["password"])
      session['id'] = person.id
      redirect_to :controller => "feeds" 
    else
      redirect_to :action => "index" 
    end
  end
  def logout
    reset_session
    redirect_to :action => "index"
  end
end
