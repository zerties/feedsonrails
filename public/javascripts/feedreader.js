function returnObjById( id )
{
  if (document.getElementById)
    var returnVar = document.getElementById(id);
  else if (document.all)
    var returnVar = document.all[id];
  else if (document.layers)
    var returnVar = document.layers[id];
  return returnVar;
}

function AjaxRequest() 
{
  var xmlHttp = null;
  // Mozilla, Opera, Safari sowie Internet Explorer 7
  if (typeof XMLHttpRequest != 'undefined') {
    xmlHttp = new XMLHttpRequest();
  }
  if (!xmlHttp) {
    // Internet Explorer 6 und älter
    try {
      xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(e) {
      try {
        xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
        xmlHttp  = null;
      }
    }
  }
  return xmlHttp;
}

