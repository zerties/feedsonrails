class Filters < ActiveRecord::Migration
  def self.up
     create_table "filters" do |t|
       t.column "owner_id", :integer
       t.column "feed_id", :integer
       t.column "title", :string
       t.column "content", :string
       t.column "link", :string
       t.column "closures", :string
     end
  end

  def self.down
    drop_table "filters"
  end
end
