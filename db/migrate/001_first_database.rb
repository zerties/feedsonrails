class FirstDatabase < ActiveRecord::Migration
  def self.up
     create_table "users" do |t|
       t.column "username", :string
       t.column "password", :string
     end
     create_table "feeds" do |t|
       t.column "title", :string
       t.column "url", :string
       t.column "link", :string
     end
     create_table "articles" do |t|
       t.column "feed_id", :integer
       t.column "title", :string
       t.column "date", :time
       t.column "url", :string
       t.column "enclosures", :string
       t.column "hash", :string
       t.column "text", :text
     end

     create_table "user_feeds", :id => false do |t|
       t.column "user", :integer
       t.column "feed", :integer
       t.column "tags", :string
       t.column "shown", :boolean
       t.column "force_title", :string
     end
     create_table "user_articles", :id => false do |t|
       t.column "user", :integer
       t.column "article", :integer
       t.column "read", :boolean
       t.column "archive", :boolean
     end

     add_index "articles", ["feed_id"]
     add_index "articles", ["date"]
     add_index "articles", ["title"]
     
     add_index "user_feeds", ["user"]
     add_index "user_feeds", ["feed"]
     
     add_index "user_articles", ["user"]
     add_index "user_articles", ["read"]
  end

  def self.down
      drop_table "users"
      drop_table "feeds"
      drop_table "articles"

      drop_table "user_feeds"
      drop_table "user_articles"
  end
end
