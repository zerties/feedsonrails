class NewTime < ActiveRecord::Migration
  def self.up
    remove_column :articles, "date"
    add_column :articles, "date", :datetime
  end

  def self.down
    remove_column :articles, "date"
    add_column :articles, "date", :time
  end
end
